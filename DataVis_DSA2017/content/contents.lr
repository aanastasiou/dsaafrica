title: About this workshop...
---
body:

# Brief message from the facilitator.

Welcome, to these web pages about data visualisation as part of the [Data Science Africa](http://www.datascienceafrica.org/dsa2017/)  conference. 

I am Athanasios Anastasiou. I received a BEng in Biomedical Engineering from the Technological Education Institute of Athens – Greece in 2002 and an MRes in Communications Engineering and Signal Processing from the University of Plymouth – UK in 2004. My professional experience includes software development with a special interest in the medical domain and digital signal processing. I am currently a lecturer in Health Data Science at Swansea University - UK. For more information, please see [this link](https://www.linkedin.com/in/athanasios).

I was delighted to have been asked to put these sessions together as part of DSA Africa 2017, primarily because I knew nothing about this region. This was a great opportunity to find out more about it by peering through available data. Granted, [Wikipedia](https://en.wikipedia.org/wiki/Tanzania) and [other sources](https://www.cia.gov/library/publications/the-world-factbook/geos/tz.html) talked about the region extensively but what I was really after, from very early on, was any initiatives for [Open Data](https://okfn.org/opendata/).

It was then very exciting to discover the breadth and depth of work around Open Data that is taking place in Tanzania and the wider region of Africa. 

Initiatives such as the [Tanzania National Bureau of Statistics](http://www.nbs.go.tz/), [Open Data Tanzania](http://opendata.go.tz/) and the [Tanzania Data Lab](https://www.dlab.or.tz/) are already hosts to a large number of data sources that *tell the story of Tanzania*. Or the story of [Kenya](http://www.opendata.go.ke/), or the story of [Nigeria](http://nigeria.opendataforafrica.org/), Malawi, Zambia, Zimbabwe, Mozambique, Egypt and others. Almost all of the African Regions today seem to have at least a section on [Data For Africa](http://dataportal.opendataforafrica.org/) and possibly several entries in great initiatives such as [Africa Open Data](https://africaopendata.org/), *"Africa's Largest Volunteer Driven Open Data Platform"*.

This was great news. So much data already available, so many stories to tell. Where do we begin?

# What is the workshop about?
This workshop is primarily all about telling the stories that emerge from datasets in a way that it is comprehensible to human beings. Usually, datasets are composed of long lists of figures, a form that makes it very difficult for human beings to see the patterns that these numbers describe. Therefore, visualisation, [the representation of data through the use of visual symbols and imagery](https://en.wikipedia.org/wiki/Visualization_(graphics)) is quickly becoming important in the availability of large numbers of datasets.

But, just as there are large numbers of datasets out there, there is also a very large number of data visualisation toolkits. Tools that make the transition from long lists of figures to visual representations very easy.

How could we make a difference?

This workshop is about those elements that make a data visualisation to stand out from typical expectations. It takes a hands on approach into defining and refining a data visualisation project from conception to publication and it applies (mostly) to static visualisations. 

It talks about principles. Principles of composition, readability and visual element choices that are elevating a typical diagram into a meaningful and comprehensible data visualisation.

The principles covered in this workshop are universal but they apply equally well to print media (e.g. Posters), web content (e.g. Websites) and even articles or books.

Ready to discover what makes a great data visualisation project?

# An overview of Tanzanian housing conditions
<p class="author"><a href="">Athanasios Anastasiou</a>, Lecturer in Health Data Science, Swansea University Medical School
</p>

## Introduction

This is a very simple report based on data from the [Tanzania Demographic and Health Survey 2010](http://opendata.go.tz/dataset/utafiti-wa-afya-ya-mama-na-mtoto-2004-2005-tanzania/resource/d151f92d-04cc-4875-b29a-7dd27dfaf361).

The objectives of the report were:

* To create a visualisation for the purposes of the Data Science Africa tutorial
* To extract some non-trivial information about Tanzania from available data

The particular focus of the report is housing conditions. That is, an
overview of the households that the surveyed individuals live in, as evidenced 
by the available data. The key questions guiding its content were:

1. Where / How do Tanzanian's live?
1. Given the living conditions, is it possible to predict an 
   individual's financial situation?
   
Housing conditions were assessed on the basis of characteristics captured 
by the survey, for each individual, referring to the house they live in.

The results of the report are divided into two parts:

1. Descriptive Statistics
    * This part provides a quick overview of the content of the dataset

1. Modeling / Dimensionality Reduction
    * This part attempts to summarise the given dataset by modeling its content
    
## Materials & Methods

### Data
The Demographic and Health Survey of 2010 was selected for its richness 
and size. It is composed of 7 tables covering data about:

1. Men's data
1. Couple's Data
1. Children's Data
1. Household Listing Data
1. **Household Data**
1. Births' Data
1. Indivual Women's Data

The original *"Household Data"* dataset contained 9623 observations and 3331 attributes.

Each row in that dataset describes an individual living in a household along with others.
Therefore, a family of three, living under the same household, would occupy three rows 
in the *Household Data* dataset.

The high number of attributes for each individual (3331) is due to the 
fact that a number of parameters are required to be recorded separately 
for each individual living in the household but also societal factors specific 
to Tanzania. For example, with a potential maximum number of 15 individuals 
living under the same roof, the field "Relationship to the Head of the Household" 
has to be expanded to 15 individual cells. 

Amongst these 3331 attributes, there were 25 attributes that described the 
house that the individuals composing a household lived in. These were as follows:

1. Cluster number
1. HOUSEHOLD number
1. Number of HOUSEHOLD members
1. Type of place of residence
1. Cluster altitude in meters
1. Source of drinking water
1. Time to get to water source
1. Type of toilet facility
1. Has electricity
1. Has radio
1. Has television
1. Has refrigerator
1. Main floor material
1. Main wall material
1. Main roof material
1. Has a landline telephone
1. Share toilet with other households
1. Type of cooking fuel
1. Anything done to water to make safe to drink
1. Number of households sharing toilet
1. Paraffin lamp
1. Iron
1. Main source of energy for lighting
1. How far is the nearest market place (kilometers)
1. How far is the nearest health facility
1. *Wealth index*
1. *Wealth index factor score (5 decimals)*

The last two attributes describe the financial situation of the household.

### Methods
The key tool used to obtain an overview of the data was the [Histogram](https://en.wikipedia.org/wiki/Histogram).

The histogram is a visual representation of a frequency distribution. A frequency distribution 
is a very simple description of the number of times a particular value appears in a dataset

This tool is very effective for the description of datasets because it applies 
both to numeric and categorical data items.

The particular focus from the point of view of describing the data was 
the distinction between living conditions in Rural and Urban areas. 
Therefore, separate histograms were produced for each region.

The key tools used for the sumarisation of the dataset were: 

* [Hierarchical Clustering](https://en.wikipedia.org/wiki/Hierarchical_clustering)
* [Multiple Linear Regression](https://en.wikipedia.org/wiki/Linear_regression#Simple_and_multiple_regression)

Hierarchical clustering is a technique that attempts to pair similar data items into progressively
larger groups that contain similar items but are different to each other. 
The similarity of any two data items is "gauged" upon their [distance](https://en.wikipedia.org/wiki/Distance#Mathematics) 
in feature space.

In other words, hierarchical clustering is going to bring together those households 
that, although remote, may be dealing with similar living conditions (House building 
method, access to electricity and communications, water purification, etc).

Multiple Linear Regression is a technique that attempts to **predict** the Wealth Index Factor Score, 
given the living conditions of a household.

In other words, this technique helps in estimating the characteristics of affluent versus 
less affluent households for the particular conditions of Tanzania.


## Results

### House construction and basic urban networks.

According to the data described by the 2010 Demogrpahics and Health Survey of Tanzania, the majority 
of Rural houses are built by "Earth, Sand, Dung|Poles and mud|Grass/thatch/mud" while the majority of 
Urban houses are built by "Cement|Sun-dried bricks|Iron sheets" with each one of these descriptors corresponds 
to "Main floor type|Main wall type|Main roof type".

![House construction by place type (Floor|Wall|Roof)](buildByPlaceType.png)

The majority of the houses do not have electricity, with the situation only slightly better for the Urban 
environment.

![](hasElectricityByPlaceType.png)

According to the same data, the majority of the population seems to be within one hour of travel time 
from the nearest water source. The condition is similar for Urban and Rural areas with the Urban 
areas characterised by distinct modes of traveling time, compared to Rural areas.

![Time to get to a water resource (min)](timeToGetToWaterSource.png)

![Time to get to a water resource by type of household (min)](timeToGetToWaterSourceByPlaceType.png)

**Question:** *What do these modes, at the Urban environment, mean?*

For the Rural population, the most common water sources are a River / lake / irrigation channel and
a public tap or an open public well, while, the Urban population obtains their water mostly from springs or 
the neighbour's tap.

![Drinking water source by type of household](drinkingWaterSourceByPlaceType.png)

Once water has been obtained, the majority of the population consumes it untreated.

![Water treatment requirement](waterTreatment.png)

However, comparatively, a larger proportion of the Urban population seems to have a 
need for water purification before consumption, compared to the rural population.

![Water treatment requirement by type of household](waterTreatmentByPlaceType.png)

**Question:** *Why is it more important to purify water in the Urban environment?*

In terms of communications, the majority of the population is not connected via 
landline telephone with the situation being slightly better in the Urban environment.

![Landline](hasLandlineByPlaceType.png)

The majority of the Rural population lives in dwellings that do not share a toilet. However, 
the exact opposite is true for the Urban population.

![](sharesToiletByPlaceType.png)

If a household does share the toilet with others, it is most commonly with another 2 households.

![Number of households sharing a toilet (when they do).](sharedToiletStats.png)

The most common type of toilet across Tanzania seems to be the "Pit Latrine / open pit". However, 
the second most popular type in the Rural populations is a "No facility / bush / field" whereas 
for the Urban population it is the "Flush to pit latrine" type.

![](typeOfToiletByPlaceType.png)


### Living conditions

According to the data decribed by the 2010 Demographics and Health Survey of Tanzania, households
are composed, most commonly, of 4 individuals with the maximum number being 15 and the minimum 
being 1. A similar trend is observed between Urban and Rural areas, although Urban living 
seems to be characterised by higher proportion of people living alone. Also of note is that 
the majority of the population seems to be living still in the Rural areas.

![Number of members living in a household](overallHouseholds.png)

![Differences between rural and urban areas](overallHouseholdsByPlaceType.png)

**Question:** *Who are these 'lonely' people?*

The majority of the population lives within 5km of a health facility. The longest distance in the 
Urban environment is approximately 5km whereas in the Rural environment it can extend up to 40km (but for 
a very small proportion of the population).

![](distanceToHealthFacility.png)

![](distanceToHealthFacilityByPlaceType.png)

Finally, the majority of the population lives within 25km of the nearest market place.
![](distanceToMarket.png)

![](distanceToMarketByPlaceType.png)


## Working out the social class division

To work out the class division, a hierarchical clustering was performed on the available features, EXCEPT 
those that are related to the wealth index.

The dendrogram was "sliced" at 3 classes to roughly correspond to the standard lower, middle, upper social 
class division. The resulting entries are available from the following list.

![](dendrogram.png)

* [Class 1](class1.csv)
* [Class 2](class2.csv)
* [Class 3](class2.csv)

## Building up a predictive model for the Wealth Index

Having access to all of these fields, it was then attempted to create 
a model in order to predict the Wealth index from individual components 
of a household.

The model was derived in two phases, using [Multiple Linear Regression](https://en.wikipedia.org/wiki/Linear_regression#Simple_and_multiple_regression).

This technique maps features from a multidimensional space to a single quantity in a linear way.

After running the model with all 25 inputs and discarding features that beared no obvious 
predicitive value (i.e. were not statistically significant), the model was run again with 12 
features.

It has to be noted here that in Multiple regression, categorical features need to be converted to 
binary (i.e. have / not have contribution) and this is why `waterSource` expands to 12 additional 
attributes.

The fitting method still warns for "colinearity" problems which are likely to be due to the coexistence 
of fields such as `hasElectricity` and `hasRefrigerator` or `hasTelevision`, because one is likely to imply 
the other.

<iframe src="summary.txt" frameborder="0"></iframe>

![Model coefficient strength](modelCoefficients.png)

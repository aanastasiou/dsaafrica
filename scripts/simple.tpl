<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8" />
    <title>Tanzania Data Demo</title>
    <link rel="stylesheet" href="style.css"/>
    <link href="https://fonts.googleapis.com/css?family=Monda" rel="stylesheet"> 
    </head>
    <body>
    <div class="toc">$toc$</div>
    $body$
    </body>
</html>

'''Athanasios Anastasiou July 2017
 The main script that downloads the data and creates all of the figures that 
 are later linked from the MD file of the report.'''

import pandas   #Required for the DataFrame data structure
from matplotlib import pyplot as plt #Required to generate plots of different types
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster #Required for the hierarchical clustering 
import statsmodels.formula.api as smf #Required for building the model that predicts the wealth index
from numpy import arange 

#The file to work on. This script was built with the 2010 survey data in mind 
#but it can run on the 2004-2005 with minimal changes.
inputFile = "./houseHoldData.csv"

#The attributes of interest to this script. (Household parameters)
columnsToKeep = ["Cluster number","HOUSEHOLD number","Month of interview","Year of interview","Date of interview (CMC)","Number of HOUSEHOLD members","Number of eligible women in HH","Number of eligible men in HH","Number of de jure members","Number of de facto members","Number of children 5 and under","Day of interview","Number of visits","Interviewer identification","Keyer identification","Region","Type of place of residence","Place of residence","Field supervisor","Field editor","Office editor","Cluster altitude in meters","Source of drinking water","NA - Source of non-drinking water","Time to get to water source","Type of toilet facility","Has electricity","Has radio","Has television","Has refrigerator","Has bicycle","Has motorcycle/scooter","Has car/truck","Main floor material","Main wall material","Main roof material","Rooms used for sleeping","Relationship structure","Sex of head of HOUSEHOLD","Age of head of HOUSEHOLD","Has a landline telephone","Share toilet with other households","Type of cooking fuel","Have bednet for sleeping","Anything done to water to make safe to drink","Water usually treated by: boil","Water usually treated by: add bleach/chlorine","Water usually treated by: strain through a cloth","Water usually treated by: use water filter","Water usually treated by: solar disinfection","Water usually treated by: let it stand and settle","Water usually treated by: other","Water usually treated by: don't know","Number of households sharing toilet","Has a mobile telephone","Has a watch","Own land usable for agriculture","Hectares for agricultural land","Owns a bank account","Wealth index","Wealth index factor score (5 decimals)","Number of mosquito nets","Number of mosquito nets with specific information","Number of children under bednet previous night","District number","Ward number","Mainland/Zanzibar","Who provides water at main source","Paraffin lamp","Iron","Main source of energy for lighting","Agricultural land owned for farming (1 decimal place)","Agricultural land owned for grazing (1 decimal place)","HH doesn't own the land","Agricultural land used for farming but not owned(1 decimal place)","Agricultural land used for grazing but not owned (1 decimal place)","How far is the nearest market place (kilometers)","How many meals per day","How many days in the past week eating meat","How many days in the past week eating fish","Problem with meeting food needs","How far is the nearest health facility","Transportation methods when going to health facility","Prepared ugali with maize flour in the past 7 days","Where got maize flour","Where bought maize flour","Brand of maize flour","Used oil for cooking in the past 7 days","Kind of oil","Where got the oil","Brand of oil"];

#The attributes that are to be taken account by clustering only.
bb=["Number of HOUSEHOLD members","Type of place of residence","Source of drinking water","Time to get to water source","Type of toilet facility","Has electricity","Has radio","Has television","Has refrigerator","Main floor material","Main wall material","Main roof material","Has a landline telephone","Share toilet with other households","Anything done to water to make safe to drink","Paraffin lamp","Iron","Main source of energy for lighting","How far is the nearest market place (kilometers)","How far is the nearest health facility"];

if __name__ == "__main__":
    #This line prevents re-reading the CSV file in memory when the script 
    #runs from an ipython console.
    if "dataIn" not in globals():
        #Read the file
        dataIn = pandas.read_csv(inputFile, index_col = False);
        #Preprocessing
        #Keep certain columns
        dataIn = dataIn[columnsToKeep];    
        #Get rid of household duplicates
        #Taking into account the cluster number (boma (?))
        #dataIn.drop_duplicates(["Cluster number", "HOUSEHOLD number"], inplace = True);
        #Taking into account just the household identifier.
        dataIn.drop_duplicates(["HOUSEHOLD number"], inplace = True);
        #Correct string labels and convert to numeric
        dataIn["Time to get to water source"].replace(["On premises","300+"],[0,600], inplace=True);
        dataIn["Time to get to water source"]=pandas.to_numeric(dataIn["Time to get to water source"]);

        dataIn["How far is the nearest market place (kilometers)"].replace(["95 or more than 95"],[150], inplace=True);
        dataIn["How far is the nearest market place (kilometers)"]=pandas.to_numeric(dataIn["How far is the nearest market place (kilometers)"]);

        #Enable this to save a CSV copy of the file that the rest of the script processes
        #dataIn.to_csv("intermediate.csv",index = False);
    
    
    #Produce figures
    #Number of people per household
    dataIn.hist(["Number of HOUSEHOLD members"],bins=dataIn["Number of HOUSEHOLD members"].max(), normed = True)
    plt.xlabel("Number of members");
    plt.ylabel("Proportion of households");
    plt.savefig("overallHouseholds.png");
    plt.close();
    #Number of people per household by place of residence
    V = dataIn.hist("Number of HOUSEHOLD members",by=["Type of place of residence"],bins=dataIn["Number of HOUSEHOLD members"].max(),normed = True, sharex = True, sharey = True)
    plt.sca(V[0]);
    plt.xlabel("Number of members");
    plt.ylabel("Porportion of households");
    plt.grid(True);
    plt.sca(V[1]);
    plt.xlabel("Number of members");
    plt.ylabel("Proportion of households");
    plt.grid(True);
    plt.savefig("overallHouseholdsByPlaceType.png");
    plt.close();
    #Time to get to the water source    
    dataIn.hist(["Time to get to water source"],bins=20, normed = True)
    plt.savefig("timeToGetToWaterSource.png")
    plt.xlabel("Time (min)");
    plt.ylabel("Proportion of households");
    plt.close();
    #Time to get to the water source by place of residence
    V = dataIn.hist("Time to get to water source", by=["Type of place of residence"],bins=20, normed = True)
    plt.sca(V[0]);
    plt.xlabel("Time (min)");
    plt.ylabel("Proportion of households");
    plt.grid(True);
    plt.sca(V[1]);
    plt.xlabel("Time (min)");
    plt.ylabel("Proportion of households");
    plt.grid(True);
    plt.tight_layout();
    plt.savefig("timeToGetToWaterSourceByPlaceType.png");
    plt.close();
        
    #Water treatment
    waterTreatment = dataIn["Anything done to water to make safe to drink"].value_counts() 
    (waterTreatment/waterTreatment.sum()).plot(kind="bar")
    plt.axes().yaxis.grid(True)
    plt.tight_layout();
    plt.savefig("waterTreatment.png");
    #Water treatment by place of residence
    F = dataIn.groupby(["Type of place of residence","Anything done to water to make safe to drink"]).size()
    plt.subplot(211);    
    plt.gcf().set_size_inches(9.25,10.5);
    (F["Rural"]/F["Rural"].sum()).plot(kind="bar")
    plt.axes().yaxis.grid(True)
    plt.title("Rural")
    plt.ylabel("Number of households")
    plt.subplot(212);
    plt.title("Urban")
    (F["Urban"]/F["Urban"].sum()).plot(kind="bar")    
    plt.axes().yaxis.grid(True)
    plt.ylabel("Number of households")
    plt.tight_layout();    
    plt.savefig("waterTreatmentByPlaceType.png")
    plt.close();
    
    #Source of drinking water
    #Tags will get re-mapped, so we have to populate even those that bear zeros
    overallCounts = dataIn["Source of drinking water"].value_counts()    
    M = dataIn[["Type of place of residence","Source of drinking water"]]
    urbanCounts = M[M["Type of place of residence"]=="Urban"]["Source of drinking water"].value_counts().to_dict()
    ruralCounts = M[M["Type of place of residence"]=="Rural"]["Source of drinking water"].value_counts().to_dict()
    oc = overallCounts.to_dict()
    T = set(urbanCounts.keys()).symmetric_difference(set(oc.keys()))
    urbanCounts.update(dict(list(zip(T,[0] * len(T)))))
    urbanCounts = pandas.Series(urbanCounts)
    T = set(ruralCounts.keys()).symmetric_difference(set(oc.keys()))
    ruralCounts.update(dict(list(zip(T,[0] * len(T)))))
    ruralCounts = pandas.Series(ruralCounts)
    overallCounts.sort_index(inplace = True)
    urbanCounts.sort_index(inplace = True)
    ruralCounts.sort_index(inplace = True)
    #Source of drinking water by type of residence    
    #The complex ends up being this complex because of the need to share
    #the x and y axis
    fig,((ax1,ax2,ax3)) = plt.subplots(nrows=3, ncols=1);
    fig.set_size_inches(9.25,10.5);
    ax1.set_ylabel("Number of households")
    ax1.set_title("Overall");
    (overallCounts/overallCounts.sum()).plot(kind="bar", ax=ax1, sharex=ax1);        
    (ruralCounts/ruralCounts.sum()).plot(kind="bar", ax=ax2, sharex=ax1)
    ax2.set_title("Rural")        
    (urbanCounts/urbanCounts.sum()).plot(kind="bar", ax=ax3, sharex=ax1)    
    ax3.set_title("Urban")
    ax1.yaxis.grid(True)
    ax2.yaxis.grid(True)
    ax3.yaxis.grid(True)
    #This resizes everything so that there is minimal overlap
    plt.tight_layout();
    plt.savefig("drinkingWaterSourceByPlaceType.png")
    plt.close();
    
    #Nearest health facility
    dataIn.hist(["How far is the nearest health facility"],bins=20, normed = True)
    plt.savefig("distanceToHealthFacility.png")
    plt.xlabel("Distance (km)");
    plt.ylabel("Proportion of households");
    plt.close();
    #Nearest health facility by place of residence
    V = dataIn.hist("How far is the nearest health facility", by=["Type of place of residence"],bins=20, normed = True)
    plt.sca(V[0]);
    plt.xlabel("Distance (km)");
    plt.ylabel("Proportion of households");
    plt.grid(True);
    plt.sca(V[1]);
    plt.xlabel("Distance (km)");
    plt.ylabel("Proportion of households");
    plt.grid(True);
    plt.tight_layout();
    plt.savefig("distanceToHealthFacilityByPlaceType.png");
    plt.close();
    
    #Nearest Market place
    dataIn.hist(["How far is the nearest market place (kilometers)"],bins=20, normed = True)
    plt.savefig("distanceToMarket.png")
    plt.xlabel("Distance (km)");
    plt.ylabel("Proportion of households");
    plt.close();
    #Nearest market place by place of residence
    V = dataIn.hist("How far is the nearest market place (kilometers)", by=["Type of place of residence"],bins=20, normed = True)
    plt.sca(V[0]);
    plt.xlabel("Distance (km)");
    plt.ylabel("Proportion of households");
    plt.grid(True);
    plt.sca(V[1]);
    plt.xlabel("Distance (km)");
    plt.ylabel("Proportion of households");
    plt.grid(True);
    plt.savefig("distanceToMarketByPlaceType.png");
    plt.close();
    
    #Housing conditions
    hc = dataIn[["Type of place of residence", "Share toilet with other households", "Has electricity", "Main floor material", "Main wall material", "Main roof material", "Type of toilet facility", "Number of households sharing toilet", "Has a landline telephone"]];
    #Due to typos, the following line does not take into account all of the data but the typos are very small in size, in comparison to the bulk of the data that is visualised here.
    shareToilet = hc[(hc["Share toilet with other households"]=="Yes") | (hc["Share toilet with other households"]=="No")]
    shareToilet = shareToilet[["Type of place of residence", "Share toilet with other households"]].groupby(["Type of place of residence","Share toilet with other households"]).size()
    
    dataIn[dataIn["Share toilet with other households"]=="Yes"].groupby("Number of households sharing toilet").size().plot(kind = "bar");
    plt.axes().yaxis.grid(True);
    plt.tight_layout();
    plt.savefig("sharedToiletStats.png");
    plt.close();
    
    typeOfToilet = hc[["Type of place of residence", "Type of toilet facility"]].groupby(["Type of place of residence","Type of toilet facility"]).size()
    hc["Has electricity"].replace(["9"],["Yes"], inplace = True);
    hasElectricity = hc[["Type of place of residence", "Has electricity"]].groupby(["Type of place of residence","Has electricity"]).size()
    hasLandline = hc[["Type of place of residence", "Has a landline telephone"]].groupby(["Type of place of residence", "Has a landline telephone"]).size();
    
    build = hc[["Type of place of residence", "Main floor material", "Main wall material", "Main roof material"]].groupby(["Type of place of residence", "Main floor material", "Main wall material", "Main roof material"]).size()
    
    #Shares toilet?
    fig,((ax1,ax2)) = plt.subplots(nrows=2, ncols=1);
    #fig.set_size_inches(9.25,10.5);
    ax1.set_ylabel("Number of households")
    ax1.set_title("Rural");
    (shareToilet["Rural"]/shareToilet["Rural"].sum()).plot(kind="bar", ax=ax1);        
    ax2.set_title("Urban")        
    (shareToilet["Urban"]/shareToilet["Urban"].sum()).plot(kind="bar", ax=ax2, sharex=ax1)   
    ax1.yaxis.grid(True)
    ax2.yaxis.grid(True) 
    #This resizes everything so that there is minimal overlap
    plt.tight_layout();
    plt.savefig("sharesToiletByPlaceType.png")
    plt.close();
    
    #Has Landline
    fig,((ax1,ax2)) = plt.subplots(nrows=2, ncols=1);
    #fig.set_size_inches(9.25,10.5);
    ax1.set_ylabel("Number of households")
    ax1.set_title("Rural");
    (hasLandline["Rural"]/hasLandline["Rural"].sum()).plot(kind="bar", ax=ax1);        
    ax2.set_title("Urban")        
    (hasLandline["Urban"]/hasLandline["Urban"].sum()).plot(kind="bar", ax=ax2, sharex=ax1)    
    ax1.yaxis.grid(True)
    ax2.yaxis.grid(True)
    #This resizes everything so that there is minimal overlap
    plt.tight_layout();
    plt.savefig("hasLandlineByPlaceType.png")
    plt.close();
    
    
    #What type of toilet?
    urbanToilet = typeOfToilet["Urban"].to_dict()
    ruralToilet = typeOfToilet["Rural"].to_dict();
    allTypes = set(urbanToilet.keys()).union(set(ruralToilet.keys()))
    
    T = set(urbanToilet.keys()).symmetric_difference(allTypes)
    urbanToilet.update(dict(list(zip(T,[0] * len(T)))))
    T = set(ruralToilet.keys()).symmetric_difference(allTypes)
    ruralToilet.update(dict(list(zip(T,[0] * len(T)))))
    
    urbanToilet = pandas.Series(urbanToilet)
    ruralToilet = pandas.Series(ruralToilet) 
        
    fig,((ax1,ax2)) = plt.subplots(nrows=2, ncols=1);
    fig.set_size_inches(9.25,10.5);
    ax1.set_ylabel("Number of households")
    ax1.set_title("Rural");
    (ruralToilet/ruralToilet.sum()).plot(kind="bar", ax=ax1);        
    ax2.set_title("Urban")        
    (urbanToilet/urbanToilet.sum()).plot(kind="bar", ax=ax2, sharex=ax1)
    ax1.yaxis.grid(True);
    ax2.yaxis.grid(True);    
    #This resizes everything so that there is minimal overlap
    plt.tight_layout();
    plt.savefig("typeOfToiletByPlaceType.png")
    plt.close();
    
    #Has electricity?
    fig,((ax1,ax2)) = plt.subplots(nrows=2, ncols=1);
    #fig.set_size_inches(9.25,10.5);
    ax1.set_ylabel("Number of households")
    ax1.set_title("Rural");
    #Normalise the plot
    (hasElectricity["Rural"]/hasElectricity["Rural"].sum()).plot(kind="bar", ax=ax1);        
    ax2.set_title("Urban")        
    (hasElectricity["Urban"]/hasElectricity["Urban"].sum()).plot(kind="bar", ax=ax2, sharex=ax1)    
    ax1.yaxis.grid(True)
    ax2.yaxis.grid(True)
    #This resizes everything so that there is minimal overlap
    plt.tight_layout();
    plt.savefig("hasElectricityByPlaceType.png")
    plt.close();
    
    #What is the most commonly encountered building type?
    fig,((ax1,ax2)) = plt.subplots(nrows=2, ncols=1);    
    fig.set_size_inches(9.25,10.5);
    ax1.set_ylabel("Number of households")
    ax1.set_title("Rural");
    ruralBuildType = build["Rural"].sort_values(ascending = True).iloc[-20:-1]
    (ruralBuildType/ruralBuildType.sum()).plot(kind="barh", ax=ax1);
    ax1.xaxis.grid(True);
    ax2.set_title("Urban")
    urbanBuildType = build["Urban"].sort_values(ascending = True).iloc[-20:-1]        
    (urbanBuildType/urbanBuildType.sum()).plot(kind="barh", ax=ax2)    
    ax2.xaxis.grid(True);
    #This resizes everything so that there is minimal overlap
    plt.tight_layout();
    plt.savefig("buildByPlaceType.png")
    plt.close();
    
    #Clustering attempt
    clusterData = dataIn[bb];
    clusterData["Type of place of residence"].replace(["Urban", "Rural"],[0,1],inplace=True)
    clusterData["Source of drinking water"].replace(clusterData["Source of drinking water"].unique(),range(0,len(clusterData["Source of drinking water"].unique())),inplace=True)
    clusterData["Type of toilet facility"].replace(clusterData["Type of toilet facility"].unique(),range(0,len(clusterData["Type of toilet facility"].unique())),inplace=True)
    clusterData["Has electricity"].replace(["Yes","No","9"],[1,0,1],inplace=True)
    clusterData["Has radio"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Has television"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Has refrigerator"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Main floor material"].replace(clusterData["Main floor material"].unique(),range(0,len(clusterData["Main floor material"].unique())),inplace=True)
    clusterData["Main wall material"].replace(clusterData["Main wall material"].unique(),range(0,len(clusterData["Main wall material"].unique())),inplace=True)
    clusterData["Main roof material"].replace(clusterData["Main roof material"].unique(),range(0,len(clusterData["Main roof material"].unique())),inplace=True)
    clusterData["Has a landline telephone"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Share toilet with other households"].replace(["Yes","No", " "],[1,0,2],inplace=True)
    clusterData["Anything done to water to make safe to drink"].replace(["Yes","No", "Don't know"],[1,0,2],inplace=True)
    clusterData["Paraffin lamp"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Iron"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Main source of energy for lighting"].replace(clusterData["Main source of energy for lighting"].unique(),range(0,len(clusterData["Main source of energy for lighting"].unique())),inplace=True)
    Z = linkage(clusterData, "ward");
    dendrogram(Z, leaf_font_size=12);
    classes = fcluster(Z,3,criterion="maxclust")
    #Pick out the two broad classes
    U = dataIn[classes==1]
    V = dataIn[classes==2]
    M = dataIn[classes==3]
    U.to_csv("class1.csv", index=False)
    V.to_csv("class2.csv", index=False);
    M.to_csv("class3.csv", index=False);
    plt.savefig("dendrogram.png");
    plt.close()
    clusterData["Wealth index factor score (5 decimals)"] = dataIn["Wealth index factor score (5 decimals)"]
    
    #Linear regression attempt
    clusterData = dataIn[bb];
    #Dummy variables
    clusterData["Has electricity"].replace(["Yes","No","9"],[1,0,1],inplace=True)
    clusterData["Has radio"].replace(["Yes","No","9"],[1,0,1],inplace=True)
    clusterData["Anything done to water to make safe to drink"].replace(["Yes","No", "Don't know","9"],[1,0,1,1],inplace=True) #Because if you don't know it is safer to assume Yes
    clusterData["Has television"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Has refrigerator"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Share toilet with other households"].replace(["Yes","No", " ","9"],[1,0,2,1],inplace=True)
    clusterData["Paraffin lamp"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    clusterData["Iron"].replace(["Yes","No", "9"],[1,0,1],inplace=True)    
    clusterData["Has a landline telephone"].replace(["Yes","No", "9"],[1,0,1],inplace=True)
    
    clusterData["residenceIsRural"] = (clusterData["Type of place of residence"]=="Rural")
    clusterData["residenceIsUrban"] = (clusterData["Type of place of residence"]=="Urban")
    
    clusterData["residenceIsUrban"] = clusterData["residenceIsUrban"].map({False:0, True:1})
    clusterData["residenceIsRural"] = clusterData["residenceIsRural"].map({False:0, True:1})
        
    clusterData["waterSourceIsOpenPublicWell"] = (clusterData["Source of drinking water"]=="Open public well")
    clusterData["waterSourceIsSpring"] = (clusterData["Source of drinking water"]=="Spring")
    clusterData["waterSourceIsRiverDam"] = (clusterData["Source of drinking water"]=="River/dam/lake/ponds/stream/canal/irirgation channel")
    clusterData["waterSourceIsPipedIntoDwelling"] = (clusterData["Source of drinking water"]=="Piped into dwelling")
    clusterData["waterSourceIsNeighborTap"] = (clusterData["Source of drinking water"]=="Neighbor's tap")
    clusterData["waterSourceIsPipedToYardPlot"] = (clusterData["Source of drinking water"]=="Piped to yard/plot")
    clusterData["waterSourceIsPublicTapStandpipe"] = (clusterData["Source of drinking water"]=="Public tap/standpipe")
    clusterData["waterSourceIsNeighborsOpenWell"] = (clusterData["Source of drinking water"]=="Neighbor's open well")
    clusterData["waterSourceIsProtectedPublicWell"] = (clusterData["Source of drinking water"]=="Protected public well")
    clusterData["waterSourceIsCartWithSmallTank"] = (clusterData["Source of drinking water"]=="Cart with small tank")
    clusterData["waterSourceIsProtectedWellInDwelling"] = (clusterData["Source of drinking water"]=="Protected well in dwelling")
    clusterData["waterSourceIsBottledWater"] = (clusterData["Source of drinking water"]=="Bottled water")
    clusterData["waterSourceIsTankerTruck"] = (clusterData["Source of drinking water"]=="Tanker truck")
    clusterData["waterSourceIsNeighborsBorehole"] = (clusterData["Source of drinking water"]=="Neighbor's borehole")
    
    clusterData["waterSourceIsOpenPublicWell"] = clusterData["waterSourceIsOpenPublicWell"].map({False:0, True:1})
    clusterData["waterSourceIsSpring"] = clusterData["waterSourceIsSpring"].map({False:0, True:1})
    clusterData["waterSourceIsRiverDam"] = clusterData["waterSourceIsRiverDam"].map({False:0, True:1})
    clusterData["waterSourceIsPipedIntoDwelling"] = clusterData["waterSourceIsPipedIntoDwelling"].map({False:0, True:1})
    clusterData["waterSourceIsNeighborTap"] = clusterData["waterSourceIsNeighborTap"].map({False:0, True:1})
    clusterData["waterSourceIsPipedToYardPlot"] = clusterData["waterSourceIsPipedToYardPlot"].map({False:0, True:1})
    clusterData["waterSourceIsPublicTapStandpipe"] = clusterData["waterSourceIsPublicTapStandpipe"].map({False:0, True:1})
    clusterData["waterSourceIsNeighborsOpenWell"] = clusterData["waterSourceIsNeighborsOpenWell"].map({False:0, True:1})
    clusterData["waterSourceIsProtectedPublicWell"] = clusterData["waterSourceIsProtectedPublicWell"].map({False:0, True:1})
    clusterData["waterSourceIsCartWithSmallTank"] = clusterData["waterSourceIsCartWithSmallTank"].map({False:0, True:1})
    clusterData["waterSourceIsProtectedWellInDwelling"] = clusterData["waterSourceIsProtectedWellInDwelling"].map({False:0, True:1})
    clusterData["waterSourceIsBottledWater"] = clusterData["waterSourceIsBottledWater"].map({False:0, True:1})
    clusterData["waterSourceIsTankerTruck"] = clusterData["waterSourceIsTankerTruck"].map({False:0, True:1})
    clusterData["waterSourceIsNeighborsBorehole"] = clusterData["waterSourceIsNeighborsBorehole"].map({False:0, True:1})
    
    clusterData["toiletFacilityIsFlushToPitLatrine"] = (clusterData["Type of toilet facility"]=="Flush - to pit latrine")
    clusterData["toiletFacilityIsPitLatrineVIP"] = (clusterData["Type of toilet facility"]=="Pit latrine - ventilated improved pit (VIP)")
    clusterData["toiletFacilityIsFlushToSepticTank"] = (clusterData["Type of toilet facility"]=="Flush - to septic tank")
    clusterData["toiletFacilityIsPitLatrineWithSlab"] = (clusterData["Type of toilet facility"]=="Pit latrine - with slab")
    clusterData["toiletFacilityIsOTHER"] = (clusterData["Type of toilet facility"]=="OTHER")
    clusterData["toiletFacilityIs99"] = (clusterData["Type of toilet facility"]=="99")
    
    clusterData["toiletFacilityIsFlushToPitLatrine"] = clusterData["toiletFacilityIsFlushToPitLatrine"].map({False:0, True:1})
    clusterData["toiletFacilityIsPitLatrineVIP"] = clusterData["toiletFacilityIsPitLatrineVIP"].map({False:0, True:1})
    clusterData["toiletFacilityIsFlushToSepticTank"] = clusterData["toiletFacilityIsFlushToSepticTank"].map({False:0, True:1})
    clusterData["toiletFacilityIsPitLatrineWithSlab"] = clusterData["toiletFacilityIsPitLatrineWithSlab"].map({False:0, True:1})
    clusterData["toiletFacilityIsOTHER"] = clusterData["toiletFacilityIsOTHER"].map({False:0, True:1})
    clusterData["toiletFacilityIs99"] = clusterData["toiletFacilityIs99"].map({False:0, True:1})
    
    
    clusterData["mainFloorMaterialIsEarth"] = (clusterData["Main floor material"] == "Earth, sand, dung")
    clusterData["mainFloorMaterialIsCement"] = (clusterData["Main floor material"] == "Cement")
    
    clusterData["mainFloorMaterialIsEarth"] = clusterData["mainFloorMaterialIsEarth"].map({False:0, True:1})
    clusterData["mainFloorMaterialIsCement"] = clusterData["mainFloorMaterialIsCement"].map({False:0, True:1})
    
    clusterData["mainWallMaterialIsSunDriedBricks"] = (clusterData["Main wall material"] == "Sun-dried bricks")
    clusterData["mainWallMaterialIsOTHER"] = (clusterData["Main wall material"] == "OTHER")
    clusterData["mainWallMaterialIsCementBlocks"] = (clusterData["Main wall material"] == "Cement blocks")
    clusterData["mainWallMaterialIsBakedBricks"] = (clusterData["Main wall material"] == "Baked bricks")
    clusterData["mainWallMaterialIsPolesAndMud"] = (clusterData["Main wall material"] == "Poles and mud")
    clusterData["mainWallMaterialIsStones"] = (clusterData["Main wall material"] == "Stones")
    clusterData["mainWallMaterialIsWoodTimber"] = (clusterData["Main wall material"] == "Wood, timber")
    clusterData["mainWallMaterialIsGrass"] = (clusterData["Main wall material"] == "Grass")
    
    clusterData["mainWallMaterialIsSunDriedBricks"] = clusterData["mainWallMaterialIsSunDriedBricks"].map({False:0, True:1})
    clusterData["mainWallMaterialIsOTHER"] = clusterData["mainWallMaterialIsOTHER"].map({False:0, True:1})
    clusterData["mainWallMaterialIsCementBlocks"] = clusterData["mainWallMaterialIsCementBlocks"].map({False:0, True:1})
    clusterData["mainWallMaterialIsBakedBricks"] = clusterData["mainWallMaterialIsBakedBricks"].map({False:0, True:1})
    clusterData["mainWallMaterialIsPolesAndMud"] = clusterData["mainWallMaterialIsPolesAndMud"].map({False:0, True:1})
    clusterData["mainWallMaterialIsStones"] = clusterData["mainWallMaterialIsStones"].map({False:0, True:1})
    clusterData["mainWallMaterialIsWoodTimber"] = clusterData["mainWallMaterialIsWoodTimber"].map({False:0, True:1})
    clusterData["mainWallMaterialIsGrass"] = clusterData["mainWallMaterialIsGrass"].map({False:0, True:1})
    
    clusterData["mainRoofMaterialIsGrassThatchMud"] = (clusterData["Main roof material"] == "Grass/thatch/mud")
    clusterData["mainRoofMaterialIsIronSheets"] = (clusterData["Main roof material"] == "Iron sheets")
    clusterData["mainRoofMaterialIsOther"] = (clusterData["Main roof material"] == "Other")

    clusterData["mainRoofMaterialIsGrassThatchMud"] = clusterData["mainRoofMaterialIsGrassThatchMud"].map({False:0, True:1})
    clusterData["mainRoofMaterialIsIronSheets"] = clusterData["mainRoofMaterialIsIronSheets"].map({False:0, True:1})
    clusterData["mainRoofMaterialIsOther"] = clusterData["mainRoofMaterialIsOther"].map({False:0, True:1})
       
    clusterData["sourceEnergyLightIsParaffinWickLamp"] = (clusterData["Main source of energy for lighting"] == "Paraffin-wick lamp")
    clusterData["sourceEnergyLightIsParaffinHurricaneLamp"] = (clusterData["Main source of energy for lighting"] == "Paraffin-hurricane lamp")
    clusterData["sourceEnergyLightIsParaffinPressureLamp"] = (clusterData["Main source of energy for lighting"] == "Paraffin-pressure lamp")
    clusterData["sourceEnergyLightIsElectricity"] = (clusterData["Main source of energy for lighting"] == "Electricity")
    clusterData["sourceEnergyLightIsSolar"] = (clusterData["Main source of energy for lighting"] == "Solar")
    clusterData["sourceEnergyLightIsFirewood"] = (clusterData["Main source of energy for lighting"] == "Firewood")
    clusterData["sourceEnergyLightIsOther"] = (clusterData["Main source of energy for lighting"] == "Other")
    clusterData["sourceEnergyLightIsCandles"] = (clusterData["Main source of energy for lighting"] == "Candles")
    
    clusterData["sourceEnergyLightIsParaffinWickLamp"] = clusterData["sourceEnergyLightIsParaffinWickLamp"].map({False:0, True:1})
    clusterData["sourceEnergyLightIsParaffinHurricaneLamp"] = clusterData["sourceEnergyLightIsParaffinHurricaneLamp"].map({False:0, True:1})
    clusterData["sourceEnergyLightIsParaffinPressureLamp"] = clusterData["sourceEnergyLightIsParaffinPressureLamp"].map({False:0, True:1})
    clusterData["sourceEnergyLightIsElectricity"] = clusterData["sourceEnergyLightIsElectricity"].map({False:0, True:1})
    clusterData["sourceEnergyLightIsSolar"] = clusterData["sourceEnergyLightIsSolar"].map({False:0, True:1})
    clusterData["sourceEnergyLightIsFirewood"] = clusterData["sourceEnergyLightIsFirewood"].map({False:0, True:1})
    clusterData["sourceEnergyLightIsOther"] = clusterData["sourceEnergyLightIsOther"].map({False:0, True:1})
    clusterData["sourceEnergyLightIsCandles"] = clusterData["sourceEnergyLightIsCandles"].map({False:0, True:1})
    
    clusterData["wealthIndex"] = dataIn["Wealth index factor score (5 decimals)"]

    clusterData.rename(columns={"Has electricity":"hasElectricity", "Has radio":"hasRadio", "Has television":"hasTelevision", "Has refrigerator":"hasRefrigerator", "Has a landline telephone":"hasLandlineTelephone", "Share toilet with other households":"sharesToilet","Anything done to water to make safe to drink":"purifiesWater","Paraffin lamp":"hasParaffinLamp", "Iron":"hasIron", "Number of HOUSEHOLD members":"nHouseholdMembers", "Time to get to water source":"timeToWaterSource", "How far is the nearest market place (kilometers)":"distanceToMarket","How far is the nearest health facility":"distanceToHealthFacility"}, inplace=True) 
    clusterData.drop(["Type of place of residence","Source of drinking water", "Type of toilet facility", "Main floor material", "Main roof material", "Main wall material", "Main source of energy for lighting" ], axis=1, inplace = True);
    
    #First attempt at the model. In this case, all parameters are taken into account
    #lm = smf.ols(formula='wealthIndex ~ nHouseholdMembers+timeToWaterSource+hasElectricity+hasRadio+hasTelevision+hasRefrigerator+hasLandlineTelephone+sharesToilet+purifiesWater+hasParaffinLamp+hasIron+distanceToMarket+distanceToHealthFacility+residenceIsRural+residenceIsUrban+waterSourceIsOpenPublicWell+waterSourceIsSpring+waterSourceIsRiverDam+waterSourceIsPipedIntoDwelling+waterSourceIsNeighborTap+waterSourceIsPipedToYardPlot+waterSourceIsPublicTapStandpipe+waterSourceIsNeighborsOpenWell+waterSourceIsProtectedPublicWell+waterSourceIsCartWithSmallTank+waterSourceIsProtectedWellInDwelling+waterSourceIsBottledWater+waterSourceIsTankerTruck+waterSourceIsNeighborsBorehole+toiletFacilityIsFlushToPitLatrine+toiletFacilityIsPitLatrineVIP+toiletFacilityIsFlushToSepticTank+toiletFacilityIsPitLatrineWithSlab+toiletFacilityIsOTHER+toiletFacilityIs99+mainFloorMaterialIsEarth+mainFloorMaterialIsCement+mainWallMaterialIsSunDriedBricks+mainWallMaterialIsOTHER+mainWallMaterialIsCementBlocks+mainWallMaterialIsBakedBricks+mainWallMaterialIsPolesAndMud+mainWallMaterialIsStones+mainWallMaterialIsWoodTimber+mainWallMaterialIsGrass+mainRoofMaterialIsGrassThatchMud+mainRoofMaterialIsIronSheets+mainRoofMaterialIsOther+sourceEnergyLightIsParaffinWickLamp+sourceEnergyLightIsParaffinHurricaneLamp+sourceEnergyLightIsParaffinPressureLamp+sourceEnergyLightIsElectricity+sourceEnergyLightIsSolar+sourceEnergyLightIsFirewood+sourceEnergyLightIsOther+sourceEnergyLightIsCandles', data=clusterData).fit()
    #Second attempt at the model. In this case, the results from the first attempt are taken into account to exclude parameters that are not statistically significant.
    #Further improvements to this would be to reduce co-varying attributes.
    lm = smf.ols(formula='wealthIndex ~ hasElectricity+hasRadio+hasTelevision+hasRefrigerator+hasLandlineTelephone+hasParaffinLamp+hasIron+residenceIsRural+waterSourceIsOpenPublicWell+waterSourceIsSpring+waterSourceIsRiverDam+waterSourceIsPipedIntoDwelling+waterSourceIsNeighborTap+waterSourceIsPublicTapStandpipe+waterSourceIsNeighborsOpenWell+waterSourceIsProtectedPublicWell+waterSourceIsCartWithSmallTank+toiletFacilityIsFlushToPitLatrine+toiletFacilityIsPitLatrineVIP+toiletFacilityIsPitLatrineWithSlab+toiletFacilityIs99+mainFloorMaterialIsEarth+mainFloorMaterialIsCement+mainWallMaterialIsSunDriedBricks+mainWallMaterialIsOTHER+mainWallMaterialIsCementBlocks+mainWallMaterialIsPolesAndMud+sourceEnergyLightIsElectricity', data=clusterData).fit()
    #Save the model data to be included in the web view
    fd = open("summary.txt","w");fd.write(str(lm.summary()));fd.close()
    #Create a graph of the coefficients
    #The following accesses the coefficient data, extracts their labels and strength 
    #sorts and plots them
    modelCoefficients = [[x[0],float(x[1])] for x in sorted(lm.summary().tables[1].data[2:], key=lambda x:float(x[1]))]
    #Create a slightly larger plot area
    fig,ax=plt.subplots()
    fig.set_size_inches(9.25,10.5);
    #Create a horizontal bar plot
    ax.barh(range(0,len(modelCoefficients)), [x[1] for x in modelCoefficients])
    plt.yticks(range(0,len(modelCoefficients)))
    ax.set_yticklabels([x[0] for x in modelCoefficients])
    ax.xaxis.grid(True);
    plt.xlabel("Coefficient Strength")    
    plt.tight_layout();
    plt.savefig("modelCoefficients.png");
    plt.close();

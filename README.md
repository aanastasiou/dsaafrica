The main repository for the preparation to [DSA 2017](http://www.datascienceafrica.org/dsa2017/)

It contains the scripts to generate the descriptive stats / modeling diagrams 
and the static website / report for the event.

#Workshops and Other Material

In addition to the source code available in this repository, the 
following material was produced at the workshops / presentations 
we had the opportunity to participate to at DSA 2017.

* [The Health Data Scientist](HDS_Presentation/NM_TZ_HDS.pptx)
* [Whiteboard pics from the Health Data Visualisation workshop](https://goo.gl/photos/CyApGGBrsacWb6sn7)
* [Whiteboard pics from the Machine Learning workshop](https://goo.gl/photos/oVAMdivkD6sBR9RE6)


#Pre-requisites (in order of requirement):

1. [Linux](https://www.ubuntu.com/)
    * The tutorial should work satisfactorily in a python environment at a 
      different architecture, with reasonable adjustments.
2. [Python](https://www.python.org/)
3. [pip](https://pip.pypa.io/en/stable/)
    * If pip is **not** installed:
        * `wget https://bootstrap.pypa.io/get-pip.py`
        * `python get-pip.py`
4. [virtualenv](https://virtualenv.pypa.io/en/stable/)
    * `pip install virtualenv`
    * For a quick introduction on what is a virtual environment and why you 
      may need it, please see [this link](http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/)

# Build process

1. Fork the repository with `git clone [URL]`. This will give you a directory `dsaafrica`.
2. `cd dsaafrica`
3. Build the virtual environment with `python virtualenv [nameOfEnvironment]`
4. Activate the virtual environment with `source [nameOfEnvironment]/bin/activate`
5. Install pre-requisites in the virtual environment with `pip install -r requirements.txt`
6. To create the static website of the tutorial: 
    * (from the root directory of `dsaafrica`) `cd DataVis_DSA2017`
    * `make build`
7. To create the report and *link it* to the static website:
    * (from the root directory of `dsaafrica`) `cd scripts`
    * `make install`
8. To see the newly built local website:
    * (from the root directory of `dsaafrica`) `firefox DataVis_DSA2017/build/index.html`

**Warning!!!** First create the static website then `make install` from the `scripts/` 
folder, otherwise, `lektor` will overwrite the `build/tzreport/` directory which is where the 
output from `scripts/` goes.

**Python 3 Users!!!*** The file `main_p3.py` is exactly the same as `main.py` but 
contains a set of modifications to make it compatible with Python 3.
    * The modifications were discovered with the [2to3](https://docs.python.org/2/library/2to3.html) python tool.
    `2to3` produces a [diff](https://linux.die.net/man/1/diff) file which is applied to `main.py` with [patch](https://linux.die.net/man/1/patch).

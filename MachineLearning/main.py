import pandas   #Required for the DataFrame data structure
from matplotlib import pyplot as plt #Required to generate plots of different types
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster #Required for the hierarchical clustering 
from numpy import arange 

inputFile = "./houseHoldData.csv"

#The attributes of interest to this script. (Household parameters)
columnsToKeep = ["Cluster number",
                 "HOUSEHOLD number",
                 "Number of HOUSEHOLD members",
                 "Number of eligible women in HH",
                 "Number of eligible men in HH",
                 "Number of de jure members",
                 "Number of de facto members",
                 "Number of children 5 and under",
                 "Region",
                 "Type of place of residence",
                 "Place of residence",
                 "Cluster altitude in meters",
                 "Source of drinking water",
                 "Time to get to water source",
                 "Type of toilet facility",
                 "Has electricity",
                 "Has radio",
                 "Has television",
                 "Has refrigerator",
                 "Main floor material",
                 "Main wall material",
                 "Main roof material",
                 "Rooms used for sleeping", 
                 "Has a landline telephone",
                 "Share toilet with other households",
                 "Type of cooking fuel",
                 "Have bednet for sleeping",
                 "Anything done to water to make safe to drink",
                 "Water usually treated by: boil",
                 "Water usually treated by: add bleach/chlorine",
                 "Water usually treated by: strain through a cloth",
                 "Water usually treated by: use water filter",
                 "Water usually treated by: solar disinfection",
                 "Water usually treated by: let it stand and settle",
                 "Water usually treated by: other",
                 #"Water usually treated by: don't know",
                 "Number of households sharing toilet",
                 "Wealth index",
                 "Wealth index factor score (5 decimals)",
                 "Mainland/Zanzibar",
                 "Who provides water at main source",
                 "Paraffin lamp",
                 "Iron",
                 "Main source of energy for lighting",
                 "How far is the nearest market place (kilometers)",
                 "How many meals per day", 
                 "How far is the nearest health facility",
                 ];
    
    
if __name__ == "__main__":
    #Read the file
    dataIn = pandas.read_csv(inputFile, index_col = False);
    #Preprocessing
    #Keep certain columns
    dataIn = dataIn[columnsToKeep];    
    #Get rid of household duplicates
    #Taking into account the cluster number (boma (?))
    #dataIn.drop_duplicates(["Cluster number", "HOUSEHOLD number"], inplace = True);
    #Taking into account just the household identifier.
    dataIn.drop_duplicates(["HOUSEHOLD number"], inplace = True);
    #Correct string labels and convert to numeric
    dataIn["Time to get to water source"].replace(["On premises","300+"],[0,600], inplace=True);
    dataIn["Time to get to water source"]=pandas.to_numeric(dataIn["Time to get to water source"]);

    dataIn["How far is the nearest market place (kilometers)"].replace(["95 or more than 95"],[150], inplace=True);
    dataIn["How far is the nearest market place (kilometers)"]=pandas.to_numeric(dataIn["How far is the nearest market place (kilometers)"]);
    
    dataIn["Who provides water at main source"].replace({"Don't know":"dunno"},inplace=True);
    
    dataIn["Source of drinking water"].replace({"Neighbor's open well":"Neighbour open well","Neighbor's tap":"Neighbour tap", "Neighbor's borehole": "Neighbour borehole"}, inplace = True)

    #Enable this to save a CSV copy of the file that the rest of the script processes
    dataIn.to_csv("intermediate.csv",index = False);
